package projetoOO;

public class Database {
	private Doencas resfriado;
	private Doencas dengue;
	private Doencas rinite;
	private Doencas asma;
	private Doencas enxaqueca;
	private Doencas sarampo;
	private Doencas herpes;
	private Doencas aids;
	private Doencas hepatite;
	private Doencas rubeola;
	

	public void DatabaseResfriado(Doencas resfriado) {
		this.resfriado = resfriado;
		this.resfriado.setNome("Resfriado");
		this.resfriado.setNomeCientifico("H1N1");
		this.resfriado.setCausa("Vírus");
		this.resfriado.setSintomas("coriza, febre, espirros");
		this.resfriado.setTratamento("Antibióticos");
	}
	public void DatabaseDengue(Doencas dengue) {
		this.dengue = dengue;
		this.dengue.setNome("Dengue");
		this.dengue.setNomeCientifico("Flaviviridae");
		this.dengue.setCausa("Vírus");
		this.dengue.setCausa("febre alta, cansaço, dores, manchas");
		this.dengue.setTratamento("Tomar muito líquido, antitérmico e intravenosa.");
	}
	public void DatabaseRinite(Doencas rinite) {
		this.rinite = rinite;
		this.rinite.setNome("Rinite");
		this.rinite.setNomeCientifico("Rinite");
		this.rinite.setCausa("Alérgeno");
		this.rinite.setSintomas("irritação no nariz, coriza, espirros, congestão nasal");
		this.rinite.setTratamento("Anti-histamínicos, corticoides, descongestionantes, imunoterapia");
	}
	public void DatabaseAsma(Doencas asma) {
		this.asma = asma;
		this.asma.setNome("Asma");
		this.asma.setNomeCientifico("Asma");
		this.asma.setCausa("Desconhecida");
		this.asma.setSintomas("tosse, respirações ofegantes");
		this.asma.setTratamento("Corticosteroides, broncodilatadores, beta-agonistas, teofilina, modificadores de leucotrienos");
	}
	public void DatabaseEnxaqueca(Doencas enxaqueca) {
		this.enxaqueca = enxaqueca;
		this.enxaqueca.setNome("Enxaqueca");
		this.enxaqueca.setNomeCientifico("Enxaqueca");
		this.enxaqueca.setCausa("Desconhecida");
		this.enxaqueca.setSintomas("Dor, náusea, foto-sensibilidade, tontura");
		this.enxaqueca.setTratamento("Neuromoduladores, betabloqueadores, antidepressivos, antivertiginosos.");
	}
	public void DatabaseSarampo(Doencas sarampo) {
		this.sarampo = sarampo;
		this.sarampo.setNome("Sarampo");
		this.sarampo.setNomeCientifico("Sarampo");
		this.sarampo.setCausa("Vírus");
		this.sarampo.setSintomas("Erupções na pele, febre alta, dor de cabeça.");
		this.sarampo.setTratamento("Anti-térmicos, repouso, ingestão de líquidos e vitamina A.");
	}
	public void DatabaseHerpes(Doencas herpes) {
		this.herpes = herpes;
		this.herpes.setNome("Herpes");
		this.herpes.setNomeCientifico("HSV1/HSV2");
		this.herpes.setCausa("Vírus");
		this.herpes.setSintomas("Áftas, nódulos, úlceras, febre, lesões.");
		this.herpes.setTratamento("Antivirais");
	}
	public void DatabaseAids(Doencas aids) {
		this.aids = aids;
		this.aids.setNome("AIDS");
		this.aids.setNomeCientifico("HIV");
		this.aids.setCausa("Vírus");
		this.aids.setSintomas("Mal funcionamento do sistema auto-imune");
		this.aids.setTratamento("Antirretrovirais");
	}
	public void DatabaseHepatite(Doencas hepatite) {
		this.hepatite = hepatite;
		this.hepatite.setNome(nome);
		this.hepatite.setNomeCientifico(nomeCientifico);
		this.hepatite.setCausa(causa);
		this.hepatite.setSintomas(sintomas);
		this.hepatite.setTratamento(tratamento);
	}
	public void DatabaseRubeola(Doencas rubeola) {
		this.rubeola = rubeola;
		this.rubeola.setNome(nome);
		this.rubeola.setNomeCientifico(nomeCientifico);
		this.rubeola.setCausa(causa);
		this.rubeola.setSintomas(sintomas);
		this.rubeola.setTratamento(tratamento);
	}
}
