package projetoOO;

public class Pessoa {
	private String nome;
	private String sobrenome;
	private String email;
	private String rg;
	private String cpf;
	private String sexo;
	private int idade;
	private String endereco[];
	
	public Pessoa(String nome, String sobrenome, String email, String rg,
			String cpf, String sexo, int idade, String[] endereco) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.email = email;
		this.rg = rg;
		this.cpf = cpf;
		this.sexo = sexo;
		this.idade = idade;
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String[] getEndereco() {
		return endereco;
	}
	public void setEndereco(String[] endereco) {
		this.endereco = endereco;
	}
}
