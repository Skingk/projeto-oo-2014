package projetoOO;

public class Doencas {
	private String nome;
	private String nomeCientifico;
	private String causa;
	private String sintomas;
	private String tratamento;
	
	public Doencas(String nome, String nomeCientifico, String causa, String sintomas,
			String tratamento) {
		this.nome = nome;
		this.nomeCientifico = nomeCientifico;
		this.causa = causa;
		this.sintomas = sintomas;
		this.tratamento = tratamento;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeCientifico() {
		return nomeCientifico;
	}
	public void setNomeCientifico(String nomeCientifico) {
		this.nomeCientifico = nomeCientifico;
	}
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	public String getSintomas() {
		return sintomas;
	}
	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
	public String getTratamento() {
		return tratamento;
	}
	public void setTratamento(String tratamento) {
		this.tratamento = tratamento;
	}
}
