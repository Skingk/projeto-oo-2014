package projetoOO;
import java.util.ArrayList;

public class Controle {
	
	private ArrayList<Pessoa> listaUsuarios;
	private Pessoa novoUsuario;
	
	public Controle(){
		this.listaUsuarios = new ArrayList<Pessoa>();
	}
		
	public void adicionar(String nome, String sobrenome, String email, String rg,
			String cpf, String sexo, int idade, String[] endereco){
		this.novoUsuario = new Pessoa(nome, sobrenome, email, rg,
				cpf, sexo, idade, endereco);
		this.listaUsuarios.add(novoUsuario);
	}
		
	public String adicionar( Pessoa novaPessoa){
		this.listaUsuarios.add(novaPessoa);
		return "Pessoa adicionada com sucesso";
	}

	public String remover( Pessoa umaPessoa ){
		this.listaUsuarios.remove(umaPessoa);
		return umaPessoa.getNome() + "Cadastro removido com sucesso!";
	}

	public Pessoa localizar( String nome){
		for(Pessoa umaPessoa: listaUsuarios){
			if( umaPessoa.getNome().equalsIgnoreCase(nome)) return umaPessoa;
		}
		return null;
	}
	
	public ArrayList<Pessoa> getListaPessoas(){
		return this.listaUsuarios;
	}
}